#!/bin/python3.11

from ast import arg
from threading import Thread
from build.hermes_py import Hermes, HermesBuffer, HermesUDP, SocketReturnCode, UDPClientAddr, announce
from time import sleep

# Global vars

## This peripheral Hermes ID
SELF_ID             = 42

## This peripheral UDP Port
SELF_PORT           = 12345

## Core Hermes ID
MOTHER_ID           = 144

## Core UDP address
MOTHER_ADDRESS      = UDPClientAddr()
MOTHER_ADDRESS.addr = "127.0.0.1"
MOTHER_ADDRESS.port = 42069

# Code
def open_hermes () -> HermesUDP | None:

    hermes = HermesUDP(SELF_ID)
    
    code = hermes.open(SELF_PORT)
    if code != SocketReturnCode.ok:
        print(f"Could not open UDP port {SELF_PORT}")
        return None
    
    suc = hermes.start()
    if not suc:
        print("Could not start Hermes UDP Threads")
        return None
    
    hermes.set_client(MOTHER_ID, MOTHER_ADDRESS)

    packet = hermes.serialize(announce, SELF_ID, [])
    hermes.write(MOTHER_ADDRESS, packet)

    print(f"Opened Hermes UDP on port {SELF_PORT} as ID {SELF_ID}")
    return hermes

def close_hermes (hermes: HermesUDP) -> None:

    print("Closing Hermes UDP")

    if hermes.hermes.queue.head != hermes.hermes.queue.tail:
        hermes.flush()
        sleep(1)

    hermes.stop()

    print("Hermes UDP was closed")

    return None

def handle_request (hermes: HermesUDP, buf: HermesBuffer):

    match (buf.command):
        case 0:
            str = buf.get_string()
            print("Hello >> " + str)
        case 1:
            str = buf.get_string()
            print("Goodbye >> " + str)
        
        case 2:
            
            def sned ():
                neo = HermesBuffer()
                neo.clear()
                neo.rewind()
                neo.set_destination(buf.remote, buf.command, True)
                neo.add_argument(input("To send : "))
                hermes.send(neo)

            t = Thread(target=sned)
            t.start()

        case 4095:
            pass

        case _:
            print(f"Got request {buf.command} from {buf.remote}")

    return None

def handle_response (hermes: HermesUDP, buf: HermesBuffer):
    # Nothing
    return None

def handle (hermes: HermesUDP) -> None:

    buf = hermes.get()
    while buf is not None:
        buf.rewind()

        if buf.is_response:
            handle_response(hermes, buf)
        else:
            handle_request(hermes, buf)

        buf = hermes.get()

    hermes.flush()
    sleep(0.1)
    return None

def main () -> None:

    # Opening Hermes
    hermes = open_hermes()
    if hermes is None:
        print("Could not open Hermes, quitting ...")
        return None
    
    run = True
    while run:
        try:
            handle(hermes)
        except KeyboardInterrupt:
            run = False
            print("\x1b[2K\rReceived ^C, quitting ...")

    # Closing Hermes
    close_hermes(hermes)

    return None

    

if __name__ == "__main__":
    main()