import unittest # https://docs.python.org/3/library/unittest.html
import build.hermes_py as hermes

class BasicTest (unittest.TestCase):

    def test_serialize (self):
        sample = (0xF7 << 21) | (0xabc << 9) | (12 << 1) | (0 << 0)

        head = hermes.Header(0xf7, 0xabc, 12, False)
        compute = int(head)

        self.assertEqual(sample, compute)

    def test_parse (self):
        sample = (0xF7 << 21) | (0xabc << 9) | (12 << 1) | (0 << 0)

        head = hermes.Header(sample)
        
        self.assertEqual(0xf7, head.remote)
        self.assertEqual(0xabc, head.command)
        self.assertEqual(12, head.sender)
        self.assertEqual(False, head.req_res)

    def test_payload (self):

        head = hermes.Header(42, 12, 144, False)
        payload = bytes(head)

        self.assertTrue(hermes.payload.valid(payload))
        self.assertTrue(hermes.payload.valid_filtered(payload, 42))
        self.assertFalse(hermes.payload.valid_filtered(payload, 144))

        b = list(payload)
        b[3] |= (1 << 7)    # Set bit 31 to 1
        payload = bytes(b)

        self.assertFalse(hermes.payload.valid(payload))

class BufferTest (unittest.TestCase):

    def test_init (self):
        
        buffer = hermes.Buffer()

        self.assertEqual(0, buffer.head.remote)
        self.assertEqual(0, buffer.head.command)
        self.assertEqual(0, buffer.head.sender)
        self.assertEqual(False, buffer.head.req_res)
        self.assertEqual(0, len(buffer))

    def test_configure (self):

        buffer = hermes.Buffer(12, 42, True, "xsodb")
        
        self.assertEqual(12, buffer.head.remote)
        self.assertEqual(42, buffer.head.command)
        self.assertEqual(0, buffer.head.sender)
        self.assertEqual(True, buffer.head.req_res)
        
        self.assertEqual("xsodb", buffer.signature)

        self.buffer = buffer

    def test_arg (self):

        buffer = hermes.Buffer(12, 42, True, "xsodb")
        buffer[:] = [ 12, "Some text ...", -7, 3.14, b'bouh' ]

        self.assertEqual(12, buffer[0])
        self.assertEqual("Some text ...", buffer[1])
        self.assertEqual(-7, buffer[2])
        self.assertEqual(3.14, buffer[3])
        self.assertEqual(b'bouh', buffer[4])
        
    def test_shrink (self):

        buffer = hermes.Buffer(12, 42, True, "xsodb")
        buffer[:] = [ 12, "Some text ...", -7, 3.14, b'bouh' ]

        before = buffer.size
        buffer[1] = "Bonjour !"
        after = buffer.size

        self.assertLess(after, before)

    def test_grow (self):

        buffer = hermes.Buffer(12, 42, True, "xsodb")
        buffer[:] = [ 12, "Some text ...", -7, 3.14, b'bouh' ]

        before = buffer.size
        buffer[4] = (123456789).to_bytes(8, "big")
        after = buffer.size

        self.assertGreater(after, before)
    
class BufferManagerTest (unittest.TestCase):

    def test_alloc (self):
        B = hermes.constants.BUFFER_COUNT
        mgr = hermes.Manager()        
        buffer0 = mgr.alloc_buffer()

        self.assertIsNotNone(buffer0)
        self.assertEqual(0, mgr.index_of(buffer0))
        self.assertEqual(0, mgr.allocated_tail)
        self.assertEqual(1, mgr.allocated_head)
        self.assertEqual(0, mgr.allocated[mgr.allocated_tail])
        self.assertEqual(B, mgr.allocated[mgr.allocated_head])
        self.assertEqual(1, mgr.allocated_buffer_count)

    def test_alloc_prioritized (self):
        B = hermes.constants.BUFFER_COUNT
        mgr = hermes.Manager()        
        buffer0 = mgr.alloc_buffer()
        buffer1 = mgr.alloc_prioritized_buffer()

        self.assertIsNotNone(buffer1)
        self.assertEqual(1, mgr.index_of(buffer1))
        self.assertEqual(B - 1, mgr.allocated_tail)
        self.assertEqual(1, mgr.allocated_head)
        self.assertEqual(1, mgr.allocated[mgr.allocated_tail])
        self.assertEqual(B, mgr.allocated[mgr.allocated_head])
        self.assertEqual(2, mgr.allocated_buffer_count)
        
    def test_free (self):
        B = hermes.constants.BUFFER_COUNT
        mgr = hermes.Manager()        
        buffer0 = mgr.alloc_buffer()
        buffer1 = mgr.alloc_prioritized_buffer()
        buffer2 = mgr.alloc_buffer()
        
        self.assertIsNotNone(buffer2)
        self.assertEqual(2, mgr.index_of(buffer2))
        self.assertEqual(2, mgr.allocated_head)
        self.assertEqual(3, mgr.allocated_buffer_count)

        # Cas où on désalloue au milieu 
        mgr.free_buffer(buffer0)

        self.assertEqual(1, mgr.allocated_head)
        self.assertEqual(B, mgr.allocated[mgr.allocated_head])
        self.assertEqual(2, mgr.allocated[0])
        
    def test_free_last (self): 
        B = hermes.constants.BUFFER_COUNT
        mgr = hermes.Manager()        
        buffer0 = mgr.alloc_buffer()
        buffer1 = mgr.alloc_prioritized_buffer()
        buffer2 = mgr.alloc_buffer()
        
        mgr.free_buffer(buffer0)
        mgr.free_last_buffer()

        self.assertEqual(0, mgr.allocated_head)
        self.assertEqual(B, mgr.allocated[mgr.allocated_head])
        self.assertEqual(1, mgr.allocated_buffer_count)

    def test_free_first (self):    
        B = hermes.constants.BUFFER_COUNT
        mgr = hermes.Manager()        
        buffer0 = mgr.alloc_buffer()
        buffer1 = mgr.alloc_prioritized_buffer()
        buffer2 = mgr.alloc_buffer()
        
        mgr.free_buffer(buffer0)
        mgr.free_last_buffer()
        mgr.free_first_buffer()

        self.assertEqual(0, mgr.allocated_tail)
        self.assertEqual(B, mgr.allocated[mgr.allocated_tail])
        self.assertEqual(0, mgr.allocated_buffer_count)

    def test_next (self):    
        B = hermes.constants.BUFFER_COUNT
        mgr = hermes.Manager()        
        buffer0 = mgr.alloc_buffer()
        buffer1 = mgr.alloc_prioritized_buffer()
        buffer2 = mgr.alloc_buffer()
        idx0 = mgr.index_of(buffer0)
        idx1 = mgr.index_of(buffer1)
        idx2 = mgr.index_of(buffer2)
        buffer0.configure(12, 5, False, "")
        buffer1.configure(12, 5, False, "")
        buffer2.configure(12, 5, False, "")

        # On marque 0 et 2 comme prêts
        mgr.mark_as_ready(buffer0)
        mgr.mark_as_ready(buffer2)

        # 0 est prioritaire sur 2 (il est plus ancien)
        self.assertEqual(idx0, mgr.next())
        mgr.free_buffer(buffer0)
        
        # 2 est le seul disponible
        self.assertEqual(idx2, mgr.next())
        mgr.free_buffer(buffer2)

        # 1 n'est pas prêt
        self.assertEqual(B, mgr.next())

    def test_find (self): 
        B = hermes.constants.BUFFER_COUNT
        mgr = hermes.Manager()        
        buffer0 = mgr.alloc_buffer()
        buffer1 = mgr.alloc_prioritized_buffer()
        buffer2 = mgr.alloc_buffer()
        idx0 = mgr.index_of(buffer0)
        idx1 = mgr.index_of(buffer1)
        idx2 = mgr.index_of(buffer2)

        # Config 0 en TX
        buffer0.head.command = 12
        buffer0.head.remote = 123

        # Config 1 en RX (clé partielle = 1)
        buffer1.head.sender = 48
        buffer1.head.command = 1
        mgr.states[idx1].key = 1
        
        # Config 2 en RX (clé partielle = 0)
        buffer2.head.sender = 48
        buffer2.head.command = 1
        mgr.states[idx2].key = 0

        # Trouve 0 car il correspond
        self.assertEqual(idx0, mgr.find_TX_buffer(123, 12))
        # Trouve 1 car prioritaire
        self.assertEqual(idx1, mgr.find_RX_buffer(48, 1))
        # Trouve 2 à cause de la clé, alors que 1 est prioritaire
        self.assertEqual(idx2, mgr.find_RX_partial(48, 1, 0))

def partial_decomp (payload: bytes):

    hi = int.from_bytes(payload[5:7], "little", signed=False)
    lo = payload[7]

    cmd = hi >> 4
    t   = ((hi & 0xf) << 8) | lo

    return cmd, t

class HermesIOTest (unittest.TestCase):

    def test_output_no_args (self):
        hms = hermes.Hermes(144)
        # Configure le buffer
        buffer = hms.outputs.alloc_buffer()
        buffer.configure(42, 12, False, "")
        buffer.head.sender = 144
        base_id = bytes(buffer.head)
        hms.outputs.mark_as_ready(buffer)
        
        # Tests
        payload = hms.output(12)
        self.assertTrue(hermes.payload.valid(payload))
        self.assertEqual(4, len(payload))
        self.assertEqual(base_id, payload[:4])
        self.assertEqual(0, hms.outputs.allocated_buffer_count)

        payload = hms.output(12)
        self.assertIsNone(payload)

    def test_output_simple (self):
        hms = hermes.Hermes(144)
        # Configure le buffer
        buffer = hms.outputs.alloc_buffer()
        buffer.configure(42, 12, False, "c")
        buffer.head.sender = 144
        buffer[0] = "Y"
        base_id = bytes(buffer.head)
        hms.outputs.mark_as_ready(buffer)
        
        # Tests
        payload = hms.output(12)
        self.assertTrue(hermes.payload.valid(payload))
        self.assertEqual(6, len(payload))
        self.assertEqual(base_id, payload[:4])
        self.assertEqual(ord('c'), payload[4])
        self.assertEqual(ord('Y'), payload[5])

        self.assertEqual(0, hms.outputs.allocated_buffer_count)
        payload = hms.output(12)
        self.assertIsNone(payload)
        
    def test_output_partial_begin (self):
        hms = hermes.Hermes(144)
        hms._partial_key = 2

        # Configure le buffer
        buffer = hms.outputs.alloc_buffer()
        buffer.configure(42, 12, False, "l")
        buffer.head.sender = 144
        buffer[0] = -1
        base_id = bytes(buffer.head)
        hms.outputs.mark_as_ready(buffer)

        partial_id = bytes(hermes.Header(
            42, hermes.constants.syscmd_partial_begin, 144, False
        ))

        payload = hms.output(12)
        self.assertTrue(hermes.payload.valid(payload))
        self.assertEqual(12, len(payload))
        self.assertNotEqual(base_id, payload[:4])
        self.assertEqual(partial_id, payload[:4])

        # Test -> ID buffer et clé partielle
        settings = buffer.head.command | (2 << 12)
        actual_settings = int.from_bytes(payload[5:7], "little", signed=False)
        self.assertEqual(settings, actual_settings)

        # Test -> longueur buffer transmis
        size = int.from_bytes(payload[8:10], "little", signed=False)
        self.assertEqual(buffer.size, size)

        self.assertEqual(1, hms.outputs.allocated_buffer_count)
        self.assertEqual(3, hms._partial_key)
        
    def test_output_partial_content (self):
        hms = hermes.Hermes(144)
        hms._partial_key = 2

        # Configure le buffer
        buffer = hms.outputs.alloc_buffer()
        buffer.configure(42, 12, False, "l")
        buffer.head.sender = 144
        buffer[0] = -1
        base_id = bytes(buffer.head)
        hms.outputs.mark_as_ready(buffer)

        # Partial Begin 
        payload = hms.output(12)

        # Partial Content
        partial_id = bytes(hermes.Header(
            42, hermes.constants.syscmd_partial_content_2, 
            144, False
        ))

        # 1er paquet
        payload = hms.output(12)
        self.assertTrue(hermes.payload.valid(payload))
        self.assertEqual(partial_id, payload[:4])
        self.assertEqual(12, len(payload))
        cmd, t = partial_decomp(payload)
        self.assertEqual(12, cmd)
        self.assertEqual(0, t)

        # 2e paquet
        payload = hms.output(12)
        self.assertTrue(hermes.payload.valid(payload))
        self.assertEqual(partial_id, payload[:4])
        self.assertEqual(12, len(payload))
        cmd, t = partial_decomp(payload)
        self.assertEqual(12, cmd)
        self.assertEqual(1, t)

        # 3e paquet
        payload = hms.output(12)
        self.assertTrue(hermes.payload.valid(payload))
        self.assertEqual(partial_id, payload[:4])
        self.assertEqual(9, len(payload))
        cmd, t = partial_decomp(payload)
        self.assertEqual(12, cmd)
        self.assertEqual(2, t)

        # Fin
        self.assertEqual(0, hms.outputs.allocated_buffer_count)
        payload = hms.output(12)
        self.assertIsNone(payload)

    def test_output_mixed_timing (self):
        hms = hermes.Hermes(144)
        hms._partial_key = 2

        A_head = hermes.Header(42, 12, 144, False)
        B_head = hermes.Header(42, hermes.constants.syscmd_heartbeat, 144, False)
        PB_head = hermes.Header(42, hermes.constants.syscmd_partial_begin, 144, False)
        PC_head = hermes.Header(42, hermes.constants.syscmd_partial_content_2, 144, False)      

        A_id = bytes(A_head)
        B_id = bytes(B_head)
        PB_id = bytes(PB_head)
        PC_id = bytes(PC_head)

        # Config. et envoi de A
        buffer = hms.outputs.alloc_buffer()
        buffer.head = A_head
        buffer.signature = "l"
        buffer[0] = -1
        hms.outputs.mark_as_ready(buffer)

        # A!
        payload = hms.output(12)
        self.assertEqual(PB_id, payload[:4])

        # A*
        payload = hms.output(12)
        self.assertEqual(PC_id, payload[:4])

        # Création de B
        buffer = hms.outputs.alloc_prioritized_buffer()
        buffer.head = B_head
        hms.outputs.mark_as_ready(buffer)

        # B
        payload = hms.output(12)
        self.assertEqual(B_id, payload[:4])

        # A*
        payload = hms.output(12)
        self.assertEqual(PC_id, payload[:4])

        # A*
        payload = hms.output(12)
        self.assertEqual(PC_id, payload[:4])

        # None
        self.assertEqual(0, hms.outputs.allocated_buffer_count)
        payload = hms.output(12)
        self.assertIsNone(payload)

    def test_input_no_args (self):
        hms = hermes.Hermes(144)

        head = hermes.Header(12, hermes.constants.syscmd_heartbeat, 144, False)
        payload = bytes(head)

        self.assertTrue(hms.input(payload))
        self.assertEqual(1, hms.inputs.allocated_buffer_count)
        self.assertEqual(1, hms.inputs.ready_buffer_count)
        self.assertEqual(0, hms.inputs.find_RX_buffer(head.sender, head.command))
        
        buffer = hms.inputs.buffers[0]
        self.assertEqual(head.remote, buffer.head.remote)
        self.assertEqual(head.sender, buffer.head.sender)
        self.assertEqual(head.command, buffer.head.command)
        self.assertEqual(head.req_res, buffer.head.req_res)

    def test_input_simple (self):
        hms = hermes.Hermes(144)

        head = hermes.Header(12, hermes.constants.syscmd_heartbeat, 144, False)
        payload = bytes(head) + b'so7\0'

        self.assertTrue(hms.input(payload))
        self.assertEqual(1, hms.inputs.allocated_buffer_count)
        self.assertEqual(1, hms.inputs.ready_buffer_count)
        self.assertEqual(0, hms.inputs.find_RX_buffer(head.sender, head.command))
        
        buffer = hms.inputs.buffers[0]
        self.assertEqual(head.remote, buffer.head.remote)
        self.assertEqual(head.sender, buffer.head.sender)
        self.assertEqual(head.command, buffer.head.command)
        self.assertEqual(head.req_res, buffer.head.req_res)

        self.assertEqual("o7", buffer[0])
        
    def test_input_partial_begin (self):
        hms = hermes.Hermes(144)
        head = hermes.Header(
            144, 0xabc, 
            12, False
        )
        pb_pl = bytes([ 
            0x18, 0xF6, 0x1F, 0x12,                             # ID
            0x78, 0xBC, 0x2A, 0x78, 0x09, 0x00                  # Contenu
        ])
        
        self.assertTrue(hms.input(pb_pl))
        self.assertEqual(1, hms.inputs.allocated_buffer_count)
        self.assertEqual(0, hms.inputs.ready_buffer_count)
        self.assertEqual(0, hms.inputs.find_RX_buffer(head.sender, head.command))
        
        buffer = hms.inputs.buffers[0]
        self.assertEqual(head.remote, buffer.head.remote)
        self.assertEqual(head.sender, buffer.head.sender)
        self.assertEqual(head.command, buffer.head.command)
        self.assertEqual(head.req_res, buffer.head.req_res)

        self.assertEqual(2, hms.inputs.states[0].key)
        self.assertEqual(0, hms.inputs.states[0].rw_pos)
       
    def test_input_partial_content (self):
        hms = hermes.Hermes(144)
        head = hermes.Header(
            144, 0xabc, 
            12, False
        )
        pb_pl = bytes([ 
            0x18, 0xF6, 0x1F, 0x12,                             # ID
            0x78, 0xBC, 0x2A, 0x78, 0x09, 0x00, 0x6f, 0x04      # Contenu
        ])
        pc_pl = [
            bytes([
                0x18, 0xFC, 0x1F, 0x12,                         # ID
                0x62, 0xC0, 0xAB, 0x00, 0x73, 0x6F, 0x37, 0x00  # Contenu
            ]),
            bytes([
                0x18, 0xFC, 0x1F, 0x12,                         # ID
                0x62, 0xC0, 0xAB, 0x01, 0x6C, 0xEF, 0xBE, 0xAD  # Contenu
            ]),
            bytes([
                0x18, 0xFC, 0x1F, 0x12,                         # ID
                0x62, 0xC0, 0xAB, 0x02, 0xDE                    # Contenu
            ])
        ]

        self.assertTrue(hms.input(pb_pl))
        self.assertEqual(1, hms.inputs.allocated_buffer_count)
        self.assertEqual(0, hms.inputs.ready_buffer_count)

        self.assertTrue(hms.input(pc_pl[1]))
        self.assertEqual(1, hms.inputs.allocated_buffer_count)
        self.assertEqual(0, hms.inputs.ready_buffer_count)
        self.assertEqual(4, hms.inputs.states[0].rw_pos)

        self.assertTrue(hms.input(pc_pl[0]))
        self.assertEqual(1, hms.inputs.allocated_buffer_count)
        self.assertEqual(0, hms.inputs.ready_buffer_count)
        self.assertEqual(8, hms.inputs.states[0].rw_pos)

        self.assertTrue(hms.input(pc_pl[2]))
        self.assertEqual(1, hms.inputs.allocated_buffer_count)
        self.assertEqual(1, hms.inputs.ready_buffer_count)
        self.assertEqual(9, hms.inputs.states[0].rw_pos)

        buffer = hms.inputs.buffers[0]
        self.assertEqual("o7", buffer[0])
        self.assertEqual(0xdeadbeef, buffer[1])

class HermesHandleTest (unittest.TestCase):

    heartbeat_request = bytes([
        0x18, 0xf4, 0x1f, 0x12
    ])
    log_response = bytes([
        0x00, 0xf0, 0x1f, 0x12, 0x6f, 0x01, 0x73, 0x6f, 0x37, 0x00
    ])
    sys_request = bytes([
        0x00, 0xee, 0x1f, 0x12
    ])
    cmd_request = bytes([
        0x00, 0x78, 0x15, 0x12
    ])

    def test_handle_input_heartbeat_request (self):

        hms = hermes.Hermes(144)
        
        # On setup les callbacks
        sys = []
        def on_sys (msg: hermes.Buffer):
            sys.append(hermes.Buffer(msg))
        hms.on_system_message = on_sys

        sent = []
        def on_send (pl: bytes):
            sent.append(pl)
        hms.on_send = on_send

        # Partie 1 - Pas de réponse automatique
        hms.auto_heartbeat = False
        hms.input(self.heartbeat_request)
        hms.handle()

        self.assertEqual(1, len(sys))
        self.assertEqual(0, len(sent))

        # Partie 2 - Réponse automatique
        hms.auto_heartbeat = True
        hms.input(self.heartbeat_request)
        hms.handle()

        self.assertEqual(1, len(sys))
        self.assertEqual(1, len(sent))

    def test_handle_input_log (self):

        hms = hermes.Hermes(144)
        
        logs = []
        def handle_log (head: hermes.Header, log: str, err: int):
            logs.append((head, log, err))
        hms.on_log = handle_log

        hms.input(self.log_response)
        hms.handle()

        self.assertEqual(1, len(logs))
        self.assertEqual("o7", logs[0][1])

    def test_handle_input_system_command (self):

        hms = hermes.Hermes(144)
        
        sys = []
        def on_sys (msg: hermes.Buffer):
            sys.append(hermes.Buffer(msg))
        hms.on_system_message = on_sys

        hms.input(self.sys_request)
        hms.handle()

        self.assertEqual(1, len(sys))
        b: hermes.Buffer = sys[0]
        self.assertEqual(hermes.constants.syscmd_manifest, b.head.command)

    def test_handle_input_command (self):
        
        hms = hermes.Hermes(144)
        
        msg = []
        def on_msg (b: hermes.Buffer):
            msg.append(hermes.Buffer(b))
        hms.on_message = on_msg

        hms.input(self.cmd_request)
        hms.handle()

        self.assertEqual(1, len(msg))
        b: hermes.Buffer = msg[0]
        self.assertEqual(0xabc, b.head.command)

    def test_handle_output (self):

        hms = hermes.Hermes(12)
        
        sent = []
        def on_send (pl: bytes):
            sent.append(pl)
        hms.on_send = on_send

        hms.request(144, hermes.constants.syscmd_heartbeat, "")
        hms.handle()

        self.assertEqual(self.heartbeat_request, sent[0])


if __name__ == "__main__":
    unittest.main()