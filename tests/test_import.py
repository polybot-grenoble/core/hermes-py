import build.hermes_py as hermes

print("Maximum buffer length :", hermes.constants.MAX_BUFFER_LEN)
print("Maximum payload length :", hermes.constants.MAX_PAYLOAD)
print("Buffer count per manager :", hermes.constants.BUFFER_COUNT)

hms = hermes.Hermes(144)

def log_handler (head: hermes.Header, message: str, error: int):
    if error > 0:
        print("\x1b[31m", end="")
    print(f"[Log from {head.sender}]", message)
    print("\x1b[0m", end="")

def send_handler (data: bytes):
    hms.input(data)

def msg_handler (data: hermes.Buffer):
    print(data.head, data.data)

hms.on_log = log_handler
hms.on_send = send_handler
hms.on_message = msg_handler

hms.send_log(12, "test")
hms.request(12, 0xabc, "s", "o7")

for i in range(10):
    hms.handle()

print("---- Pont ----")
print()

hms2 = hermes.Hermes(42)
b = hermes.Bridge(hms2)

def cb0 (pl: bytes):
    print("CB0", len(pl), pl)

def cb1 (pl: bytes):
    print("CB1", len(pl), pl)

def cb2 (pl: bytes):
    print("CB2", len(pl), pl)

b.set_callback(0, 12,  cb0)
b.set_callback(1, 24, cb1)
b.set_callback(2, 64, cb2)

# print("---- Depuis 2 ----")

hms.on_send = lambda data : b.input(2, data)

hms.request(12, 0xabc, "s", "o7")
hms.send_log(12, "test du pont Hermès")

for i in range(10):
    hms.handle()
    b.handle()

print("---- Depuis 0 ----")

b.unset_callback(0)

hms.on_send = lambda data : b.input(0, data)

# hms.request(12, 0xabc, "s", "o7")
hms.send_log(12, "test du pont Hermès")

for i in range(10):
    hms.handle()
    b.handle()
