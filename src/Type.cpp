#include "Type.hpp"

namespace Hermes::Py {

void bindTypes (py::module m) {

    auto c = py::class_<Type>(m, "type");

    py::enum_<HermesType>(c, "types")
        .value("none",      HermesT_none)
        .value("char",      HermesT_char)
        .value("int8",      HermesT_int8)
        .value("int16",     HermesT_int16)
        .value("int32",     HermesT_int32)
        .value("int64",     HermesT_int64)
        .value("float",     HermesT_float)
        .value("double",    HermesT_double)
        .value("string",    HermesT_string)
        .value("blob",      HermesT_blob)
        .export_values();

}

}