#include "Buffer.hpp"

namespace Hermes::Py {


namespace Buffer {
namespace Docs {

const char *constructor0 =
    "Crée un Buffer."
;

const char *constructor1 =
    "Crée un Buffer.\n"
    "\n"
    "Args:\n"
    "    remote: Identifiant du destinataire\n"
    "    command: Identifiant de la commande\n"
    "    req_res: Drapeau Requête/Réponse\n"
    "    signature: Signature des arguments du buffer\n"
;

const char *constructor2 =
    "Copie un Buffer.\n\nArgs:\n    buffer: Le buffer à copier"
;

const char *configure =
    "Configure un Buffer.\n"
    "\n"
    "Args:\n"
    "    remote: Identifiant du destinataire\n"
    "    command: Identifiant de la commande\n"
    "    req_res: Drapeau Requête/Réponse\n"
    "    signature: Signature des arguments du buffer\n"
;

}

std::shared_ptr<HermesBuffer> constructor0 () {
    auto self = std::make_shared<HermesBuffer>();
    HermesBuffer_clear(self.get());
    return self;
}

std::shared_ptr<HermesBuffer> constructor1 (
    HermesID remote, 
    HermesCommand command, 
    bool req_res, 
    std::string signature
) {
    auto self = std::make_shared<HermesBuffer>();
    HermesBuffer_clear(self.get());
    HermesBuffer_configure(
        self.get(), 
        remote, command, req_res, signature.c_str()
    );
    return self;
}

std::shared_ptr<HermesBuffer> constructor2 (HermesBuffer& buffer) {
    auto self = std::make_shared<HermesBuffer>();
    memcpy(self.get(), &buffer, sizeof(HermesBuffer));
    return self;
}


py::bytes get_data (HermesBuffer& buffer) {
    return py::bytes((const char *)buffer.data, buffer.length);
}

void set_data (HermesBuffer& buffer, py::bytes& data) {
    py::buffer_info info(py::buffer(data).request());
    const char *bytes = reinterpret_cast<const char *>(info.ptr);
    size_t length = static_cast<size_t>(info.size);

    if (length > HERMES_MAX_BUFFER_LEN) {
        throw std::out_of_range("Buffer cannot hold that much bytes !");
    }

    buffer.length = length;
    for (size_t i = 0; i < length; i++) {
        buffer.data[i] = bytes[i];
    }
}

std::string get_signature (HermesBuffer& buffer) { // Juste de la gestion de string ...
    auto len = HermesBuffer_argCount(&buffer);
    char *temp = new char[len + 1];
    HermesBuffer_getSignature(&buffer, temp);
    temp[len] = 0;
    std::string out (temp);
    delete []temp;
    return out;
}

void set_signature (HermesBuffer& buffer, std::string signature) {
    HermesBuffer_setSignature(&buffer, signature.c_str());
}

std::string __repr__ (HermesBuffer &buffer) {
    std::string out = "<";

    // Head
    out += "(";
    out += std::to_string(buffer.head.remote)   + ", ";
    out += std::to_string(buffer.head.command)  + ", ";
    out += std::to_string(buffer.head.sender)   + ", ";
    out += std::to_string(buffer.head.req_res);
    out += ") ";

    // Data
    out += std::to_string(buffer.length) + " bytes, ";

    // Signature
    auto len = HermesBuffer_argCount(&buffer);
    char *signature = new char[len + 1];
    signature[len] = 0;
    HermesBuffer_getSignature(&buffer, signature);

    out += "signature: '" + std::string(signature) + "'";
    delete []signature;

    out += ">";
    return out;
}

}

void bindBuffer (py::module m) {

    auto c = py::class_<HermesBuffer, std::shared_ptr<HermesBuffer>>(
        m, "Buffer"
    );
        
    /* -- Constructeurs -- */
    c.def(py::init<>(&Buffer::constructor0), Buffer::Docs::constructor0);

    c.def(
        py::init(&Buffer::constructor1),
        py::arg("remote"), py::arg("command"), py::arg("req_res"),
        py::arg("signature"),
        Buffer::Docs::constructor1
    );

    c.def(py::init(&Buffer::constructor2), Buffer::Docs::constructor2);

    /* -- Propriétés -- */
    c.def_readwrite("head", &HermesBuffer::head);

    c.def("__len__", [](HermesBuffer &buffer) { 
        return HermesBuffer_argCount(&buffer); 
    });

    c.def_property(
        "data", &Buffer::get_data, &Buffer::set_data
    );

    c.def_property("signature", &Buffer::get_signature, &Buffer::set_signature);

    c.def_property_readonly(
        "size", [](HermesBuffer& buffer) { return buffer.length; }
    );

    /* -- Méthodes -- */
    c.def(
        "configure", &HermesBuffer_configure,
        py::arg("remote"), py::arg("command"), py::arg("req_res"),
        py::arg("signature"),
        Buffer::Docs::configure
    );

    c.def("__getitem__", &getBufferArgument);
    c.def("__setitem__", &setBufferArgument);

    c.def("__getitem__", &getBufferArguments);
    c.def("__setitem__", &setBufferArguments);

    c.def("__repr__", &Buffer::__repr__);

}

/**
 * @brief Récupère un argument dans le buffer, et le retourne avec le bon 
 * cast. 
 * 
 * Cette fonction est DANGEREUSE et n'est donc pas rendue publique.
 * 
 * @tparam T Type du cast
 * @tparam type Type d'argument Hermès attendu
 * @param loc Pointeur vers l'argument
 * @return T L'argument
 */
template<typename T, HermesType type> 
T getBufferArg (uint8_t* loc) {
    
    if (*loc != type) {
        throw std::invalid_argument("Wrong argument cast");
    }

    uint8_t *data = loc + 1;
    T argument;

    size_t s = HermesType_size((HermesType)*loc);
    memcpy(&argument, data, s);

    return argument;

}

/**
 * @brief Récupère un argument dans le buffer, et le retourne avec le bon 
 * cast. 
 * 
 * Cette fonction est DANGEREUSE et n'est donc pas rendue publique.
 * 
 * @param loc Pointeur vers l'argument
 * @return std::string L'argument
 */
std::string getBufferStr (uint8_t* loc) {
        
    if (*loc != HermesT_string) {
        throw std::invalid_argument("Wrong argument cast");
    }

    const char *data = (const char *)(loc + 1);
    size_t s = (1 + strlen(data)) * sizeof(char);
    
    char* temp = new char[s];
    memcpy(temp, data, s);

    std::string out (temp);
    delete []temp;

    return out;

}

/**
 * @brief Récupère un argument dans le buffer, et le retourne avec le bon 
 * cast. 
 * 
 * Cette fonction est DANGEREUSE et n'est donc pas rendue publique.
 * 
 * @param buffer le Buffer
 * @param loc Pointeur vers l'argument
 * @return py::bytes L'argument
 */
py::bytes getBufferBlob (HermesBuffer& buffer, uint8_t* loc) {
        
    if (*loc != HermesT_blob) {
        throw std::invalid_argument("Wrong argument cast");
    }

    const char *data = (const char *)(loc + 1);
    size_t s = HermesBuffer_dataEnd(&buffer) - (loc + 1);
    
    return py::bytes(data, s);

}

py::object getBufferArgument (HermesBuffer& buffer, uint16_t index) {

    uint8_t *loc = HermesBuffer_argLoc(&buffer, index);

    if (loc == HermesBuffer_dataEnd(&buffer)) {
        throw std::out_of_range("Argument index out of range");
    }

    py::object obj;
    switch ((HermesType)*loc)
    {
    case HermesT_char:
        obj = py::cast(getBufferArg<char, HermesT_char>(loc));
        break;

    case HermesT_int8:
        obj = py::cast(getBufferArg<int8_t, HermesT_int8>(loc));
        break;
    case HermesT_int16:
        obj = py::cast(getBufferArg<int16_t, HermesT_int16>(loc));
        break;
    case HermesT_int32:
        obj = py::cast(getBufferArg<int32_t, HermesT_int32>(loc));
        break;
    case HermesT_int64:
        obj = py::cast(getBufferArg<int64_t, HermesT_int64>(loc));
        break;

    case HermesT_float:
        obj = py::cast(getBufferArg<float, HermesT_float>(loc));
        break;
    case HermesT_double:
        obj = py::cast(getBufferArg<double, HermesT_double>(loc));
        break;

    case HermesT_string:
        obj = py::cast(getBufferStr(loc));
        break;

    case HermesT_blob:
        obj = getBufferBlob(buffer, loc);
        break;

    case HermesT_none:
    default:
        obj = py::none();
        break;
    }

    return obj;

}


void setBufferArgument (HermesBuffer& buffer, uint16_t index, py::object obj) {
    
    uint8_t *loc = HermesBuffer_argLoc(&buffer, index);

    if (loc == HermesBuffer_dataEnd(&buffer)) {
        throw std::out_of_range("Argument index out of range");
    }

    switch ((HermesType)*loc) {

        case HermesT_char: {
            char c = obj.cast<char>();
            if (!HermesBuffer_set(&buffer, index, &c)) 
                throw std::runtime_error("Failed to write to buffer");
            break;
        }
        
        case HermesT_int8: {
            int8_t c = obj.cast<int8_t>();
            if (!HermesBuffer_set(&buffer, index, &c))
                throw std::runtime_error("Failed to write to buffer");
            break;
        }
        case HermesT_int16: {
            int16_t c = obj.cast<int16_t>();
            if (!HermesBuffer_set(&buffer, index, &c))
                throw std::runtime_error("Failed to write to buffer");
            break;
        }
        case HermesT_int32: {
            int32_t c = obj.cast<int32_t>();
            if (!HermesBuffer_set(&buffer, index, &c))
                throw std::runtime_error("Failed to write to buffer");
            break;
        }
        case HermesT_int64: {
            int64_t c = obj.cast<int64_t>();
            if (!HermesBuffer_set(&buffer, index, &c))
                throw std::runtime_error("Failed to write to buffer");
            break;
        }

        case HermesT_float: {
            float c = obj.cast<float>();
            if (!HermesBuffer_set(&buffer, index, &c))
                throw std::runtime_error("Failed to write to buffer");
            break;
        }
        case HermesT_double: {
            double c = obj.cast<double>();
            if (!HermesBuffer_set(&buffer, index, &c))
                throw std::runtime_error("Failed to write to buffer");
            break;
        }
        
        case HermesT_string: {
            std::string str = obj.cast<std::string>();
            if (!HermesBuffer_set(&buffer, index, str.c_str()))
                throw std::runtime_error("Failed to write to buffer");
            break;
        }
        case HermesT_blob: {
            py::buffer_info info(py::buffer(obj).request());
            const char *bytes = reinterpret_cast<const char *>(info.ptr);
            size_t length = static_cast<size_t>(info.size);

            if (!HermesBuffer_setBlob(&buffer, index, length, (void*)bytes))
                throw std::runtime_error("Failed to write to buffer");
            break;
        }

        case HermesT_none:
        default:
            break;

    }

}

// https://github.com/pybind/pybind11/issues/1095#issuecomment-330327626
py::list getBufferArguments (HermesBuffer& buffer, py::slice slice) {

    size_t start, stop, step, slicelength;
    auto count = HermesBuffer_argCount(&buffer);
    if (!slice.compute(count, &start, &stop, &step, &slicelength))
        throw py::error_already_set();

    py::list result;
    for (size_t i = 0; i < slicelength; ++i) {
        py::object item = getBufferArgument(buffer, start);
        result.append(item);
        start += step;
    }
    return result;

}

void setBufferArguments (HermesBuffer& buffer, py::slice slice, py::list list) {

    size_t start, stop, step, slicelength;
    auto count = HermesBuffer_argCount(&buffer);

    if (!slice.compute(count, &start, &stop, &step, &slicelength))
        throw py::error_already_set();

    if (count < list.size()) {
        throw std::runtime_error("List too long");
    }

    for (size_t i = 0; i < slicelength; ++i) {
        py::object elem = list[i];
        setBufferArgument(buffer, start, elem);
        start += step;
    }

}


}