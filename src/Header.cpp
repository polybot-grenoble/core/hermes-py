#include "Header.hpp"

namespace Hermes::Py {

namespace Header::Docs {

const char *constructor0 = 
    "Crée un objet HermesHeader.\n"
    "\n"
    "Initialise toutes les valeurs à zéro.\n"
;

const char *constructor1 = 
    "Copie un objet HermesHeader.\n"
    "\n"
    "Args:\n"
    "    header: L'en-tête à copier\n"
;

const char *constructor2 = 
    "Crée un objet HermesHeader.\n"
    "\n"
    "Args:\n"
    "    header: L'en-tête à copier sous forme d'entier non signé\n"
;

const char *constructor3 = 
    "Crée un objet HermesHeader.\n"
    "\n"
    "Args:\n"
    "    remote: L'identifiant du destinataire\n"
    "    command: L'identifiant de la commande\n"
    "    sender: L'identifiant de l'envoyeur\n"
    "    req_res: Drapeau Requête/Réponse\n"
;

const char *remote = "Identifiant du destinataire";
const char *command = "Identifiant de la commande";
const char *sender = "Identifiant de l'envoyeur";
const char *req_res = "Drapeau Requête/Réponse";

const char *__int__ = "Convertit un HermesHeader en entier non signé.";
const char *__bytes__ = "Convertit un HermesHeader en suite d'octets.";
const char *__repr__ = "Crée un représentation textuelle du HermesHeader.";

}

namespace Header {

std::shared_ptr<HermesHeader> constructor0 () {
    auto head = std::make_shared<HermesHeader>();
    head->remote    = 0;
    head->command   = 0;
    head->sender    = 0;
    head->req_res   = false;
    return head;
}

std::shared_ptr<HermesHeader> constructor1 (HermesHeader &header) {
    auto self = std::make_shared<HermesHeader>();
    self->remote  = header.remote;
    self->command = header.command;
    self->sender  = header.sender;
    self->req_res = header.req_res;
    return self;
}

std::shared_ptr<HermesHeader> constructor2 (uint32_t& header) {
    auto self = std::make_shared<HermesHeader>();
    *self = HermesHeader_parse(header);
    return self;
}

std::shared_ptr<HermesHeader> constructor3 (
    HermesID remote, HermesCommand command, HermesID sender, bool req_res
) {
    auto self = std::make_shared<HermesHeader>();
    self->remote    = remote;
    self->command   = command;
    self->sender    = sender;
    self->req_res   = req_res;
    return self;
}

uint32_t __int__ (HermesHeader& header) { 
    return HermesHeader_serialize(header);
}

py::bytes __bytes__ (HermesHeader& header) { 
    char id[sizeof(uint32_t)+1];
    // Cast magic
    *(uint32_t*)id = HermesHeader_serialize(header);
    id[sizeof(uint32_t)] = 0;
    return py::bytes(id, sizeof(uint32_t));
}

std::string __repr__ (HermesHeader& header) {
    std::string out = "<";

    out += "remote: " + std::to_string((int)header.remote) + ", ";
    out += "command: " + std::to_string(header.command) + ", ";
    out += "sender: " + std::to_string((int)header.sender) + ", ";
    out += "req_res: " + std::to_string(header.req_res);

    out += ">";
    return out;
}

}

void bindHeader (py::module m) {

    auto c = py::class_<HermesHeader, std::shared_ptr<HermesHeader>>(
        m, "Header"
    );

    /* -- Constructeurs -- */
    // https://pybind11.readthedocs.io/en/stable/advanced/classes.html#custom-constructors

    c.def(py::init(&Header::constructor0), Header::Docs::constructor0);

    c.def(
        py::init(&Header::constructor1), 
        py::arg("header"), 
        Header::Docs::constructor1
    );
    
    c.def(
        py::init(&Header::constructor2), 
        py::arg("header"), 
        Header::Docs::constructor2
    );

    c.def(
        py::init(&Header::constructor3), 
        py::arg("remote"), py::arg("command"), 
        py::arg("sender"), py::arg("req_res"), 
        Header::Docs::constructor3
    );

    /* -- Propriétés -- */
    c.def_readwrite("remote",  &HermesHeader::remote,   Header::Docs::remote);
    c.def_readwrite("command", &HermesHeader::command,  Header::Docs::command);
    c.def_readwrite("sender",  &HermesHeader::sender,   Header::Docs::sender);
    c.def_readwrite("req_res", &HermesHeader::req_res,  Header::Docs::req_res);

    /* -- Méthodes -- */
    c.def("__int__", &Header::__int__, Header::Docs::__int__);       
    c.def("__bytes__", &Header::__bytes__, Header::Docs::__bytes__);   
    c.def("__repr__", &Header::__repr__, Header::Docs::__repr__);

}


}