#include "Payload.hpp"

namespace Hermes::Py {

namespace Payload::Docs {
    
const char *valid = 
    "Vérifie qu'un tronçon de données est valide.\n"
    "\n"
    "Un tronçon respecte le format :\n"
    " - Longueur >= 4\n"
    " - Bits 29 à 31 = 0\n"
    " - ID != 0\n"
    "\n"
    "Args:\n"
    "    bytes: Le tronçon sous forme d'octets\n"
    "\n"
    "Returns:\n"
    "    Vrai si le tronçon vérifie les conditions, faux sinon.\n"
    "\n"
    "Raises:\n"
    "    Runtime Error: Le tronçon est trop long pour l'espace\n"
    "    mémoire qui lui est réservé (flag de compilation Hermès)."
;

const char *validFiltered = 
    "Vérifie qu'un tronçon de données est valide.\n"
    "\n"
    "Un tronçon respecte le format :\n"
    " - Longueur >= 4\n"
    " - Bits 29 à 31 = 0\n"
    " - ID != 0\n"
    " - remote == self\n"
    "\n"
    "Args:\n"
    "    bytes: Le tronçon sous forme d'octets\n"
    "    self: L'identifiant de récepteur\n"
    "\n"
    "Returns:\n"
    "    Vrai si le tronçon vérifie les conditions, faux sinon.\n"
    "\n"
    "Raises:\n"
    "    Runtime Error: Le tronçon est trop long pour l'espace\n"
    "    mémoire qui lui est réservé (flag de compilation Hermès)."
;

}


HermesPayload payloadFromBytes (py::bytes bytes) {
    
    py::buffer_info info(py::buffer(bytes).request());
    const char *rawBytes = reinterpret_cast<const char *>(info.ptr);
    size_t length = static_cast<size_t>(info.size);

    if (length > HERMES_MAX_PAYLOAD) {
        throw std::runtime_error("Too much bytes for payload");
    }

    HermesPayload p;
    p.length = length;
    memcpy(p.data, rawBytes, length);

    return p;

}

py::bytes payloadToBytes (HermesPayload &payload) {

    return py::bytes((const char *)payload.data, payload.length);

}

void bindPayload (py::module m) {
    
    py::class_<payload>(m, "payload")
        .def_static("valid", [](py::bytes data) {
            auto p = payloadFromBytes(data);
            return HermesPayload_valid(&p);
        }, py::arg("bytes"), Payload::Docs::valid)
        .def_static("valid_filtered", [](py::bytes data, uint8_t self) {
            auto p = payloadFromBytes(data);
            return HermesPayload_validFiltered(&p, self);
        }, py::arg("bytes"), py::arg("self"), Payload::Docs::validFiltered)
        ;

}   
    
} // namespace Hermes::Py
