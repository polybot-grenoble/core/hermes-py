#include "Hermes.hpp"
#include "Payload.hpp"

namespace Hermes::Py {

namespace Hermes {

std::shared_ptr<Hermes_t> constructor0(HermesID id) {
    auto ptr = std::make_shared<Hermes_t>();
    Hermes_init(ptr.get(), id);

    // Initialisation du gestionnaire de callbacks C++
    // Fuite mémoire ici ^^
    // Je n'ai pas vraiment le choix, Python c'est nul
    ptr->slot = new Callbacks();
    ptr->logCallback = &Callbacks::handle_log;
    ptr->send = &Callbacks::handle_send;
    ptr->onMessage = &Callbacks::handle_onMessage;
    ptr->onSystemMessage = &Callbacks::handle_onSystemMessage;

    return ptr;
}

bool input (Hermes_t &hermes, py::bytes payload) {

    // Conversion bytes -> payload
    HermesPayload temp = payloadFromBytes(payload);
    
    // Vérification de la validité 
    if (!HermesPayload_valid(&temp)) {
        throw std::runtime_error("Invalid payload from bytes");
    }

    return Hermes_input(&hermes, temp);

}

std::optional<py::bytes> output (Hermes_t& hermes, uint16_t maxLen) {

    if (maxLen < 12) {
        throw std::runtime_error("max_len should be >= 12");
    } 
    
    HermesPayload obj = Hermes_output(&hermes, maxLen);

    if (!HermesPayload_valid(&obj)) {
        return std::nullopt;
    }

    return payloadToBytes(obj);

}

bool get_auto_heartbeat(Hermes_t* hermes) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    return cb->auto_heartbeat;
}

void set_auto_heartbeat(Hermes_t* hermes, bool value) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    cb->auto_heartbeat = value;
}

std::optional<
    std::function<void(HermesHeader, std::string, uint8_t)>
> get_on_log (Hermes_t* hermes) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    if (cb->log != nullptr) {
        return cb->log;
    } 
    return std::nullopt;
}

void set_on_log (
    Hermes_t* hermes, 
    std::function<void(HermesHeader, std::string, uint8_t)> f
) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    cb->log = f;
}


std::optional<std::function<void(py::bytes)>> get_on_send (Hermes_t* hermes) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    if (cb->send != nullptr) {
        return cb->send;
    } 
    return std::nullopt;
}

void set_on_send (
    Hermes_t* hermes, 
    std::function<void(py::bytes)> f
) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    cb->send = f;
}


std::optional<
    std::function<void(HermesBuffer*)>
> get_on_message (Hermes_t* hermes) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    if (cb->onMessage != nullptr) {
        return cb->onMessage;
    } 
    return std::nullopt;
}

void set_on_message (
    Hermes_t* hermes, 
    std::function<void(HermesBuffer*)> f
) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    cb->onMessage = f;
}


std::optional<
    std::function<void(HermesBuffer*)>
> get_on_system_message (Hermes_t* hermes) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    if (cb->onSystemMessage != nullptr) {
        return cb->onSystemMessage;
    } 
    return std::nullopt;
}

void set_on_system_message (
    Hermes_t* hermes, 
    std::function<void(HermesBuffer*)> f
) {
    Callbacks* cb = (Callbacks*)hermes->slot;
    cb->onSystemMessage = f;
}

/**
 * @brief Implémentation pour les objets python des fonctions `Hermes_request`
 * et `Hermes_respond`
 */
bool request_respond (
    Hermes_t* hermes, HermesID remote, HermesCommand cmd, std::string signature,
    bool req_res, py::args args
) {

    HermesBuffer* buffer = HermesBufferManager_allocBuffer(&hermes->output);
    if (buffer == NULL) {
        throw std::runtime_error("Output buffer full");
    }

    HermesBuffer_configure(buffer, remote, cmd, req_res, signature.c_str());
    buffer->head.sender = hermes->id;

    uint16_t count = HermesBuffer_argCount(buffer);
    if (py::len(args) != count) {
        throw std::length_error("Invalid argument count");
    }

    for (uint16_t i = 0; i < count; i++) {

        switch (signature[i])
        {
        case HermesT_char: {
            char c = args[i].cast<char>();
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int8: {
            int8_t c = args[i].cast<int8_t>();
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int16: {
            int16_t c = args[i].cast<int16_t>();
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int32: {
            int32_t c = args[i].cast<int32_t>();
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int64: {
            int64_t c = args[i].cast<int64_t>();
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_double: {
            double c = args[i].cast<double>();
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_float: {
            float c = args[i].cast<float>();
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_string: {
            std::string s = args[i].cast<std::string>();
            HermesBuffer_set(buffer, i, s.c_str());
            break;
        }
        // Blob n'est pas accepté ici
        case HermesT_blob:
            throw std::runtime_error("Can't assign blob via request or respond");

        default:
            break;
        }

    }

    HermesBufferManager_markAsReady(&hermes->output, buffer);
    return true;

}

bool request (
    Hermes_t* hermes, HermesID remote, HermesCommand cmd, std::string signature,
    py::args args
) {
    return request_respond(hermes, remote, cmd, signature, false, args);
}

bool respond (
    Hermes_t* hermes, HermesID remote, HermesCommand cmd, std::string signature,
    py::args args
) {
    return request_respond(hermes, remote, cmd, signature, true, args);
}

namespace Docs {

const char *constructor0 = 
    "Initialise une instance Hermès.\n\n"
    "Args:\n    id: Identifiant Hermès de cette instance.\n";

const char *input = 
    "Récupère et traite le fragment. \nN'effectue pas de vérification sur "
    "la validité du fragment et ne filtre pas sur l'identifiant de récepteur. "
    "Ces vérifications doivent être effectués en amont.\n" 
    "Cette fonction retourne faux si il n'y a pas de buffer disponible.\n\n"
    "Args:\n"
    "    payload: Les données lues\n"
    "Ne peut pas dépasser `constants.MAX_PAYLOAD`.\n"
    "\nReturns:\n"
    "   True si l'entrée est traitée, False il n'y a plus de place disponible."
    "\n\nRaises:\n"
    "    RuntimeError(\"Too much bytes for payload\"): "
    "Erreur à la conversion bytes -> payload, trop d'octets.\n"
    "    RuntimeError(\"Invalid payload from bytes\"): "
    "Erreur à la conversion bytes -> payload, fromat invalide.\n";

const char *output = 
    "Donne un fragment à envoyer sur le support. "
    "Si il n'y a rien à envoyer, le fragment sera None.\n\n"
    "Args:\n"
    "    max_len: Longueur maximale autorisée sur le support. "
    "Ne peut pas dépasser `constants.MAX_PAYLOAD`.\n"
    "\nReturns:\n"
    "    Les octets à envoyer ou None";

const char *request = 
    "Envoie une requête à un périphérique. NON BLOQUANT.\n"
    "\nArgs:\n"
    "    remote: Identifiant du récepteur\n"
    "    cmd: Identifiant de commande\n"
    "    signature: Signature des arguments\n"
    "    *args: Valeurs des arguments\n"
    "\nReturns:\n"
    "    True si l'envoi est fait, False si les buffer de sortie sont pleins."
    ;

const char *respond = 
    "Envoie une réponse à un périphérique. NON BLOQUANT.\n"
    "\nArgs:\n"
    "    remote: Identifiant du récepteur\n"
    "    cmd: Identifiant de commande\n"
    "    signature: Signature des arguments\n"
    "    *args: Valeurs des arguments\n"
    "\nReturns:\n"
    "    True si l'envoi est fait, False si les buffer de sortie sont pleins."
    ;


}

}

void bindHermes(py::module m) {

    auto hms = py::class_<Hermes_t, std::shared_ptr<Hermes_t>>(m, "Hermes");

    // Constructeur
    hms.def(
        py::init(&Hermes::constructor0), 
        py::arg("id"), Hermes::Docs::constructor0
    );

    // Propriétés
    hms.def_readwrite("id", &Hermes_t::id);
    hms.def_readonly(
        "inputs", &Hermes_t::input, 
        py::return_value_policy::reference_internal
    );
    hms.def_readonly(
        "outputs", &Hermes_t::output,
        py::return_value_policy::reference_internal
    );
    hms.def_readwrite("_partial_key", &Hermes_t::partialKey);
    hms.def_readwrite(
        "default_payload_length", &Hermes_t::defaultPayloadLength
    );

    hms.def_property(
        "auto_heartbeat",
        &Hermes::get_auto_heartbeat,
        &Hermes::set_auto_heartbeat
    );

    hms.def_property("on_log", &Hermes::get_on_log, &Hermes::set_on_log);
    hms.def_property("on_send", &Hermes::get_on_send, &Hermes::set_on_send);
    hms.def_property(
        "on_message", 
        &Hermes::get_on_message, &Hermes::set_on_message
    );
    hms.def_property(
        "on_system_message", 
        &Hermes::get_on_system_message, &Hermes::set_on_system_message
    );    


    // Méthodes
    hms.def(
        "input", &Hermes::input, 
        py::arg("payload"),
        Hermes::Docs::input
    );

    hms.def(
        "output", &Hermes::output,
        py::arg("max_len"),
        Hermes::Docs::output
    );

    hms.def(
        "request", &Hermes::request,
        py::arg("remote"), py::arg("cmd"), py::arg("signature"), 
        Hermes::Docs::request
    );

    hms.def(
        "respond", &Hermes::respond,
        py::arg("remote"), py::arg("cmd"), py::arg("signature"), 
        Hermes::Docs::respond
    );

    hms.def(
        "init_TX", &Hermes_initTX
    );

    hms.def(
        "send", &Hermes_send,
        py::arg("buffer")
    );

    hms.def(
        "handle", &Hermes_handle
    );

    // Raccourcis
    hms.def(
        "send_log", &Hermes_log,
        py::arg("id"), py::arg("message"), py::arg("error") = 0
    );

}

}