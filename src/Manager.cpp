#include "Manager.hpp"

namespace Hermes::Py {

namespace BufferState {

std::shared_ptr<HermesBufferState> constructor0 () {
    auto s = std::make_shared<HermesBufferState>();
    s->key = 4;
    s->ready = false;
    s->rw_pos = HERMES_MAX_BUFFER_LEN;
    return s;
}

std::shared_ptr<HermesBufferState> constructor1 (HermesBufferState &state) {
    auto s = std::make_shared<HermesBufferState>();
    s->key = state.key;
    s->ready = state.ready;
    s->rw_pos = state.rw_pos;
    return s;
}

};

namespace Manager {

std::shared_ptr<HermesBufferManager> constructor0 () {
    auto m = std::make_shared<HermesBufferManager>();
    HermesBufferManager_init(m.get());
    return m;
}

std::vector<HermesBuffer*> buffers (HermesBufferManager& manager) {
    std::vector<HermesBuffer*> vec (HERMES_BUFFER_COUNT);
    for (auto i = 0; i < HERMES_BUFFER_COUNT; i++) {
        vec[i] = &manager.buffers[i];
    }
    return vec;
}

std::vector<HermesBufferState*> states (HermesBufferManager& manager) {
    std::vector<HermesBufferState*> vec (HERMES_BUFFER_COUNT);
    for (auto i = 0; i < HERMES_BUFFER_COUNT; i++) {
        vec[i] = &manager.states[i];
    }
    return vec;
}

std::vector<uint8_t*> allocated (HermesBufferManager& manager) {
    std::vector<uint8_t*> vec (HERMES_BUFFER_COUNT);
    for (auto i = 0; i < HERMES_BUFFER_COUNT; i++) {
        vec[i] = &manager.allocated[i];
    }
    return vec;
}

std::string __repr__ (HermesBufferManager& manager) {

    std::string out = "<";

    out += "allocated: ";
    out += std::to_string(HermesBufferManager_allocatedBufferCount(&manager));
    out += ", free: ";
    out += std::to_string(HermesBufferManager_freeBufferCount(&manager));
    out += ", ready: ";
    out += std::to_string(HermesBufferManager_readyBufferCount(&manager));

    out += ">";
    return out;

}

namespace Docs {

const char* constructor0 = "Crée un HermesBufferManager.";

const char* alloc_buffer = 
    "Alloue un buffer et retourne son pointeur. Si aucun buffer n'est"
    " disponible, la fonction retourne None.\n\n"
    "Returns:\n    Un buffer, None si aucun disponible.\n";

const char* alloc_prioritized_buffer = 
    "Alloue un buffer prioritaire et retourne son pointeur. Si aucun buffer "
    "n'est disponible, la fonction retourne None.\n\n"
    "Returns:\n    Un buffer, None si aucun disponible.\n";

const char* free_last_buffer = "Libère le dernier buffer de la file d'attente.";
const char* free_first_buffer = "Libère le premier buffer de la file d'attente.";
const char* free_buffer = "Libère un buffer associé à un gestionnaire.";

const char* index_of = 
    "Donne l'indice correspondant à un pointeur d'un buffer."
    " Si le buffer n'existe pas dans le gestionnaire, la fonction retourne" 
    " `constants.BUFFER_COUNT`.\n\n"
    "Args:\n    buffer: le buffer dont on veut l'indice\n"
    "Returns:\n"
    "    L'indice du buffer, `constants.BUFFER_COUNT` s'il n'existe pas\n";

const char* next = 
    "Donne l'indice du premier buffer prêt. Si aucun buffer n'est prêt, "
    "la fonction renvoie `constants.HERMES_BUFFER_COUNT`.\n\n"
    "Returns:\n"
    "    L'indice du premier buffer prêt, `constants.HERMES_BUFFER_COUNT`"
    " si aucun buffer ne l'est.\n";

const char* find_RX_buffer = 
    "Donne l'indice du buffer correspondant aux filtres. Si aucun buffer "
    "ne correspond, la fonction renvoie `constants.HERMES_BUFFER_COUNT`.\n\n"
    "Args:\n"
    "    sender: ID de l'envoyeur\n"
    "    command: ID de la commande\n"
    "\nReturns:\n    "
    "L'indice du buffer correspondant, `constants.HERMES_BUFFER_COUNT` sinon.";

const char* find_TX_buffer = 
    "Donne l'indice du buffer correspondant aux filtres. Si aucun buffer "
    "ne correspond, la fonction renvoie `constants.HERMES_BUFFER_COUNT`.\n\n"
    "Args:\n"
    "    sender: ID du destinataire\n"
    "    command: ID de la commande\n"
    "\nReturns:\n    "
    "L'indice du buffer correspondant, `constants.HERMES_BUFFER_COUNT` sinon.";

const char* find_RX_partial = 
    "Donne l'indice du buffer correspondant aux filtres. Si aucun buffer "
    "ne correspond, la fonction renvoie `constants.HERMES_BUFFER_COUNT`.\n\n"
    "Args:\n"
    "    sender: ID de l'envoyeur\n"
    "    command: ID de la commande\n"
    "    key: Clé de transmission partielle\n"
    "\nReturns:\n    "
    "L'indice du buffer correspondant, `constants.HERMES_BUFFER_COUNT` sinon.";

const char* mark_as_ready = 
    "Marque un buffer géré comme étant prêt\n\n"
    "Args:\n"
    "    buffer: le buffer";

}

}

void bindManager(py::module m) {

    auto state = py::class_<
            HermesBufferState, std::shared_ptr<HermesBufferState>
        > (m, "buffer_state");

    state.def(py::init(&BufferState::constructor0));
    state.def(py::init(&BufferState::constructor1));
    state.def_readwrite("rw_pos", &HermesBufferState::rw_pos);
    state.def_readwrite("ready", &HermesBufferState::ready);
    state.def_readwrite("key", &HermesBufferState::key);

    auto mgr = py::class_<
            HermesBufferManager, std::shared_ptr<HermesBufferManager>
        >(m, "Manager");

    // Constructeurs
    mgr.def(py::init(&Manager::constructor0));

    // Propriétés
    mgr.def_readonly("allocated_head", &HermesBufferManager::aHead);
    mgr.def_readonly("allocated_tail", &HermesBufferManager::aTail);
    mgr.def_property_readonly(
        "buffers", &Manager::buffers,
        py::return_value_policy::reference_internal
    );
    mgr.def_property_readonly(
        "states", &Manager::states,
        py::return_value_policy::reference_internal
    );
    mgr.def_property_readonly(
        "allocated", &Manager::allocated,
        py::return_value_policy::reference_internal
    );

    // Méthodes
    // - Alloc
    mgr.def_property_readonly(
        "allocated_buffer_count", &HermesBufferManager_allocatedBufferCount
    );
    mgr.def_property_readonly(
        "free_buffer_count", &HermesBufferManager_freeBufferCount
    );
    mgr.def_property_readonly(
        "ready_buffer_count", &HermesBufferManager_readyBufferCount
    );

    mgr.def(
        "alloc_buffer", 
        [](HermesBufferManager& manager) -> std::optional<HermesBuffer*> {
            auto ptr = HermesBufferManager_allocBuffer(&manager);
            if (ptr == nullptr) {
                return std::nullopt;
            }
            return ptr;
        }, 
        Manager::Docs::alloc_buffer,
        py::return_value_policy::reference_internal
    );
    mgr.def(
        "alloc_prioritized_buffer", 
        [](HermesBufferManager& manager) -> std::optional<HermesBuffer*> {
            auto ptr = HermesBufferManager_allocPrioritizedBuffer(&manager);
            if (ptr == nullptr) {
                return std::nullopt;
            }
            return ptr;
        },
        Manager::Docs::alloc_prioritized_buffer,
        py::return_value_policy::reference_internal
    );

    
    // - Free
    mgr.def(
        "free_last_buffer", &HermesBufferManager_freeLastBuffer,
        Manager::Docs::free_last_buffer
    );
    mgr.def(
        "free_first_buffer", &HermesBufferManager_freeFirstBuffer,
        Manager::Docs::free_first_buffer
    );
    mgr.def(
        "free_buffer", &HermesBufferManager_freeBuffer,
        py::arg("buffer"),
        Manager::Docs::free_buffer
    );

    // - Recherche
    mgr.def(
        "index_of", &HermesBufferManager_indexOf,
        py::arg("buffer"),
        Manager::Docs::index_of
    );
    mgr.def(
        "next", &HermesBufferManager_nextBuffer,
        Manager::Docs::next
    );
    mgr.def(
        "find_RX_buffer", &HermesBufferManager_findRXBuffer,
        py::arg("sender"), py::arg("command"),
        Manager::Docs::find_RX_buffer
    );
    mgr.def(
        "find_TX_buffer", &HermesBufferManager_findTXBuffer,
        py::arg("remote"), py::arg("command"),
        Manager::Docs::find_TX_buffer
    );
    mgr.def(
        "find_RX_partial", &HermesBufferManager_findRXPartial,
        py::arg("remote"), py::arg("command"), py::arg("key"),
        Manager::Docs::find_RX_partial
    );

    // - Autres
    mgr.def(
        "mark_as_ready", &HermesBufferManager_markAsReady,
        py::arg("buffer"),
        Manager::Docs::mark_as_ready
    );

    mgr.def("__repr__", &Manager::__repr__);

}

}