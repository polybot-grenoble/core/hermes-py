#include "Bridge.hpp"
#include <functional>
#include <pybind11/functional.h>
#include "Payload.hpp"

namespace Hermes::Py {

namespace Bridge {

namespace Docs {

const char* constructor0 = "";

const char* handle = "";

const char* input = "";

const char* set_callback = "";

const char* unset_callback = "";

}

// Callback pour le pont
void bridgeCallback (void* arg, HermesPayload payload) {

    auto f = reinterpret_cast<std::function<void(py::bytes)>*>(arg);
    auto temp = payloadToBytes(payload);

    (*f)(temp);

}

std::shared_ptr<HermesBridge> constructor0 (Hermes_t& hermes) {

    auto ptr = std::make_shared<HermesBridge>();
    HermesBridge_init(ptr.get(), &hermes);

    return ptr;

}

void input (HermesBridge& bridge, uint8_t source, py::bytes payload) {

    HermesPayload temp = payloadFromBytes(payload);
    
    HermesBridge_input(&bridge, temp, source);

}

void set_callback (
    HermesBridge& bridge, uint8_t n, uint16_t chunkSize,
    std::function<void(py::bytes)> callback
) {

    std::function<void(py::bytes)>* F;

    if (n >= 4) throw std::range_error("Max. 4 callbacks");

    if (bridge.outputs[n].enabled) {
        F = reinterpret_cast<std::function<void(py::bytes)>*>(
            bridge.outputs[n].slot
        );
        delete F;
    }

    F = new std::function<void(py::bytes)>(callback);
    bridge.outputs[n] = {
        .enabled = true,
        .chunkSize = chunkSize,
        .callback = &bridgeCallback,
        .slot = F
    };

}

void unset_callback (HermesBridge& bridge, uint8_t source) {

    if (source >= 4) throw std::range_error("Max. 4 callbacks");

    if (bridge.outputs[source].enabled) {
        auto F = reinterpret_cast<std::function<void(py::bytes)>*>(
            bridge.outputs[source].slot
        );
        delete F;
    }

    bridge.outputs[source].enabled = false;

}


}

void bindBridge (py::module m) {

    auto b = py::class_<HermesBridge, std::shared_ptr<HermesBridge>>(
        m, "Bridge"
    );

    b.def(
        py::init(&Bridge::constructor0), 
        py::arg("hermes"), Bridge::Docs::constructor0
    );

    b.def("handle", &HermesBridge_handle, Bridge::Docs::handle);

    b.def(
        "input", &Bridge::input, 
        py::arg("source"), py::arg("payload"),
        Bridge::Docs::input
    );

    b.def(
        "set_callback", &Bridge::set_callback,
        py::arg("source"), py::arg("chunk_size"), py::arg("callback"),
        Bridge::Docs::set_callback
    );

    b.def(
        "unset_callback", &Bridge::unset_callback,
        py::arg("source"),
        Bridge::Docs::unset_callback
    );

}

}