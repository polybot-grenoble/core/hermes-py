#include "Constants.hpp"

namespace Hermes::Py {

void bindConstants (py::module m) {
    
    auto c = py::class_<Constants>(m, "constants");    

    c.attr("MAX_BUFFER_LEN") = HERMES_MAX_BUFFER_LEN;
    c.attr("MAX_PAYLOAD")    = HERMES_MAX_PAYLOAD;
    c.attr("BUFFER_COUNT")   = HERMES_BUFFER_COUNT;

    py::enum_<enum HermesSystemCmd>(c, "system_cmd")
        .value(
            "syscmd_partial_content_3",   
            (HermesSystemCmd)(HermesSysCmd_PartialContent | 3)
        )
        .value(
            "syscmd_partial_content_2",   
            (HermesSystemCmd)(HermesSysCmd_PartialContent | 2)
        )
        .value(
            "syscmd_partial_content_1",   
            (HermesSystemCmd)(HermesSysCmd_PartialContent | 1)
        )
        .value(
            "syscmd_partial_content_0",   
            (HermesSystemCmd)(HermesSysCmd_PartialContent | 0)
        )
        .value("syscmd_partial_content",   HermesSysCmd_PartialContent)
        .value("syscmd_partial_begin",     HermesSysCmd_PartialBegin)
        .value("syscmd_heartbeat",         HermesSysCmd_Heartbeat)
        .value("syscmd_error",             HermesSysCmd_Error)
        .value("syscmd_log",               HermesSysCmd_Log)
        .value("syscmd_manifest",          HermesSysCmd_Manifest)
        .value("syscmd_manifest_chunk",    HermesSysCmd_ManifestChunk)
        .value("syscmd_system_cmd",        HermesSysCmd)
        .export_values();

}

}