#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

extern "C" {
    #include <hermes/include/HermesBuffer.h>
    #include <hermes/include/HermesBridge.h>
}

#include <string>
#include <vector>
#include <stdexcept>

namespace py = pybind11;

namespace Hermes::Py {

    void bindBridge (py::module m);

}