#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

extern "C" {
    #include <hermes/include/Constants.h>
}

namespace py = pybind11;

namespace Hermes::Py {

    class Constants {
        
    };

    void bindConstants (py::module m);

}
