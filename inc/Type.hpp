#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

extern "C" {
    #include <hermes/include/HermesType.h>
}

namespace py = pybind11;

namespace Hermes::Py {

    class Type {

    };

    void bindTypes (py::module m);

}