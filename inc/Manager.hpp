#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

extern "C" {
    #include <hermes/include/HermesBuffer.h>
    #include <hermes/include/HermesBufferManager.h>
}

#include <string>
#include <optional>

namespace py = pybind11;

namespace Hermes::Py {

    void bindManager(py::module m);

}