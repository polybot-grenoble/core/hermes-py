#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

extern "C" {
    #include <hermes/include/HermesBuffer.h>
}

#include <string>
#include <vector>
#include <stdexcept>

namespace py = pybind11;

namespace Hermes::Py {

    void bindBuffer (py::module m);
    
    /**
     * @brief Récupère un argument dans le buffer, et le retourne avec le bon 
     * cast. 
     * 
     * @param buffer Le buffer 
     * @param index L'indice
     * @return py::object L'argument
     */
    py::object getBufferArgument (HermesBuffer& buffer, uint16_t index);

    /**
     * @brief Définit un argument dans le buffer en demandant le cast de la 
     * part de pybind11.
     * 
     * @param buffer Le buffer
     * @param index L'indice de l'argument
     * @param obj La nouvelle valeur de l'argument
     */
    void setBufferArgument (
        HermesBuffer& buffer, uint16_t index, py::object obj
    );

    // Pareil, mais avec des slices
    py::list getBufferArguments (HermesBuffer& buffer, py::slice slice);

    void setBufferArguments (
        HermesBuffer& buffer, py::slice slice, py::list list
    );

}