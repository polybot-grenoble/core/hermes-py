#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

extern "C" {
    #include <hermes/include/HermesBuffer.h>
}

#include <string>

namespace py = pybind11;

namespace Hermes::Py {

    /** Définit la classe Header dans le module Python */
    void bindHeader (py::module m);

}