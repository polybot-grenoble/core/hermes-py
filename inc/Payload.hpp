#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

extern "C" {
    #include <hermes/include/HermesBuffer.h>
}

#include <string>

namespace py = pybind11;

namespace Hermes::Py {

    class payload {  };

    HermesPayload payloadFromBytes (py::bytes bytes);

    py::bytes payloadToBytes (HermesPayload &payload);

    void bindPayload (py::module m);

}