#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

extern "C" {
    #include <hermes/include/Hermes.h>
}

#include <string>
#include <optional>
#include <functional>
#include <vector>

#include "Payload.hpp"

namespace py = pybind11;

namespace Hermes::Py {

    namespace Hermes {

    class Callbacks {

    public:
        bool auto_heartbeat = true;

        std::function<void(HermesHeader, std::string, uint8_t)> log = nullptr;
        std::function<void(py::bytes)> send = nullptr;
        std::function<void(HermesBuffer*)> onMessage = nullptr;
        std::function<void(HermesBuffer*)> onSystemMessage = nullptr;

        static void handle_log (
            Hermes_t* hermes, 
            HermesHeader head, const char* message, uint8_t errorCode
        ) {
            Callbacks* cb = (Callbacks*)hermes->slot;
            if (cb->log != nullptr) {
                cb->log(head, std::string(message), errorCode);
            }
        }

        static void handle_send (Hermes_t* hermes, HermesPayload payload) {
            Callbacks* cb = (Callbacks*)hermes->slot;
            if (cb->send != nullptr) {
                auto bytes = payloadToBytes(payload);
                cb->send(bytes);
            }
        }

        static void handle_onMessage (Hermes_t* hermes, HermesBuffer* buffer) {
            Callbacks* cb = (Callbacks*)hermes->slot;
            if (cb->onMessage != nullptr) {
                cb->onMessage(buffer);
            }
        }
        
        static void handle_onSystemMessage (
            Hermes_t* hermes, HermesBuffer* buffer
        ) {
            Callbacks* cb = (Callbacks*)hermes->slot;
            
            // Auto-heartbeat
            if (
                cb->auto_heartbeat && 
                buffer->head.command == HermesSysCmd_Heartbeat
            ) {
                Hermes_heartbeat(hermes, buffer->head.sender, true);

            // Normal handle
            } else if (cb->onSystemMessage != nullptr) {
                cb->onSystemMessage(buffer);
            }
        }

    };

    };

    void bindHermes(py::module m);

}