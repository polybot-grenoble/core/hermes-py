# Hermes-py

A barebones wrapper for Hermes to use it with Python.

The lack of documentation is due to a lack of time to properly do it.

You may generate the stubs for the module using `stubgen.sh` to have 
intellisense working in VSCode using this module.

## Depedencies

You will need Python3 to be installed.

To generate the stubs, you will need mypy.

```sh
python3 -m pip install mypy
```

You will also need the C++ build tools and CMake.

For example on Ubuntu/Debian:
```sh
sudo apt install build-essential libc++-dev cmake
```

## Roadmap

- \[X] Hermes.c
- \[ ] SocketCAN.cpp
- \[ ] HermesCAN.cpp
- \[X] UDPSocket.cpp
- \[X] HermesUDP.cpp
 
## How to use

Step 1: build the thing

```sh
mkdir build
cd build
cmake ..
make
```

Step 2: Use the thing

The module is called `hermes-py.cpython-...so`. Import it with a relative path.

`test.py` is an example to test the wrapper for Hermes.c

`test_udp.py` is an example to test the wrapper for HermesUDP (and UDPSocket, 
since it inherits from)

`toolbox.py` creates the few functions that can't be wrapped.

It does not compile ? GL;HF ! (Or try contacting me @VulcanixFR)
