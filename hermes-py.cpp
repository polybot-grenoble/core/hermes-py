#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

#include <functional>

#include <iostream>

#include "Constants.hpp"
#include "Type.hpp"
#include "Header.hpp"
#include "Buffer.hpp"
#include "Payload.hpp"
#include "Manager.hpp"
#include "Hermes.hpp"
#include "Bridge.hpp"

namespace py = pybind11;

PYBIND11_MODULE(hermes_py, m) {
    
    Hermes::Py::bindConstants(m);
    Hermes::Py::bindTypes(m);
    Hermes::Py::bindHeader(m);
    Hermes::Py::bindBuffer(m);
    Hermes::Py::bindPayload(m);
    Hermes::Py::bindManager(m);
    Hermes::Py::bindHermes(m);
    Hermes::Py::bindBridge(m);

}