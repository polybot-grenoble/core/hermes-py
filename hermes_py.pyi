import typing
from typing import Callable, ClassVar, overload

class Bridge:
    def __init__(self, hermes: Hermes) -> None:
        """__init__(self: hermes_py.Bridge, hermes: hermes_py.Hermes) -> None"""
    def handle(self) -> None:
        """handle(self: hermes_py.Bridge) -> None"""
    def input(self, source: int, payload: bytes) -> None:
        """input(self: hermes_py.Bridge, source: int, payload: bytes) -> None"""
    def set_callback(self, source: int, chunk_size: int, callback) -> None:
        """set_callback(self: hermes_py.Bridge, source: int, chunk_size: int, callback: std::function<void __cdecl(bytes)>) -> None"""
    def unset_callback(self, source: int) -> None:
        """unset_callback(self: hermes_py.Bridge, source: int) -> None"""

class Buffer:
    data: bytes
    head: Header
    signature: str
    @overload
    def __init__(self) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.Buffer) -> None

        Crée un Buffer.

        2. __init__(self: hermes_py.Buffer, remote: int, command: int, req_res: bool, signature: str) -> None

        Crée un Buffer.

        Args:
            remote: Identifiant du destinataire
            command: Identifiant de la commande
            req_res: Drapeau Requête/Réponse
            signature: Signature des arguments du buffer


        3. __init__(self: hermes_py.Buffer, arg0: hermes_py.Buffer) -> None

        Copie un Buffer.

        Args:
            buffer: Le buffer à copier
        """
    @overload
    def __init__(self, remote: int, command: int, req_res: bool, signature: str) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.Buffer) -> None

        Crée un Buffer.

        2. __init__(self: hermes_py.Buffer, remote: int, command: int, req_res: bool, signature: str) -> None

        Crée un Buffer.

        Args:
            remote: Identifiant du destinataire
            command: Identifiant de la commande
            req_res: Drapeau Requête/Réponse
            signature: Signature des arguments du buffer


        3. __init__(self: hermes_py.Buffer, arg0: hermes_py.Buffer) -> None

        Copie un Buffer.

        Args:
            buffer: Le buffer à copier
        """
    @overload
    def __init__(self, arg0: Buffer) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.Buffer) -> None

        Crée un Buffer.

        2. __init__(self: hermes_py.Buffer, remote: int, command: int, req_res: bool, signature: str) -> None

        Crée un Buffer.

        Args:
            remote: Identifiant du destinataire
            command: Identifiant de la commande
            req_res: Drapeau Requête/Réponse
            signature: Signature des arguments du buffer


        3. __init__(self: hermes_py.Buffer, arg0: hermes_py.Buffer) -> None

        Copie un Buffer.

        Args:
            buffer: Le buffer à copier
        """
    def configure(self, remote: int, command: int, req_res: bool, signature: str) -> None:
        """configure(self: hermes_py.Buffer, remote: int, command: int, req_res: bool, signature: str) -> None

        Configure un Buffer.

        Args:
            remote: Identifiant du destinataire
            command: Identifiant de la commande
            req_res: Drapeau Requête/Réponse
            signature: Signature des arguments du buffer

        """
    @overload
    def __getitem__(self, arg0: int) -> object:
        """__getitem__(*args, **kwargs)
        Overloaded function.

        1. __getitem__(self: hermes_py.Buffer, arg0: int) -> object

        2. __getitem__(self: hermes_py.Buffer, arg0: slice) -> list
        """
    @overload
    def __getitem__(self, arg0: slice) -> list:
        """__getitem__(*args, **kwargs)
        Overloaded function.

        1. __getitem__(self: hermes_py.Buffer, arg0: int) -> object

        2. __getitem__(self: hermes_py.Buffer, arg0: slice) -> list
        """
    def __iter__(self) -> typing.Iterator[object]:
        """def __iter__(self) -> typing.Iterator[object]"""
    def __len__(self) -> int:
        """__len__(self: hermes_py.Buffer) -> int"""
    @overload
    def __setitem__(self, arg0: int, arg1: object) -> None:
        """__setitem__(*args, **kwargs)
        Overloaded function.

        1. __setitem__(self: hermes_py.Buffer, arg0: int, arg1: object) -> None

        2. __setitem__(self: hermes_py.Buffer, arg0: slice, arg1: list) -> None
        """
    @overload
    def __setitem__(self, arg0: slice, arg1: list) -> None:
        """__setitem__(*args, **kwargs)
        Overloaded function.

        1. __setitem__(self: hermes_py.Buffer, arg0: int, arg1: object) -> None

        2. __setitem__(self: hermes_py.Buffer, arg0: slice, arg1: list) -> None
        """
    @property
    def size(self) -> int: ...

class Header:
    command: int
    remote: int
    req_res: bool
    sender: int
    @overload
    def __init__(self) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.Header) -> None

        Crée un objet HermesHeader.

        Initialise toutes les valeurs à zéro.


        2. __init__(self: hermes_py.Header, header: hermes_py.Header) -> None

        Copie un objet HermesHeader.

        Args:
            header: L'en-tête à copier


        3. __init__(self: hermes_py.Header, header: int) -> None

        Crée un objet HermesHeader.

        Args:
            header: L'en-tête à copier sous forme d'entier non signé


        4. __init__(self: hermes_py.Header, remote: int, command: int, sender: int, req_res: bool) -> None

        Crée un objet HermesHeader.

        Args:
            remote: L'identifiant du destinataire
            command: L'identifiant de la commande
            sender: L'identifiant de l'envoyeur
            req_res: Drapeau Requête/Réponse

        """
    @overload
    def __init__(self, header: Header) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.Header) -> None

        Crée un objet HermesHeader.

        Initialise toutes les valeurs à zéro.


        2. __init__(self: hermes_py.Header, header: hermes_py.Header) -> None

        Copie un objet HermesHeader.

        Args:
            header: L'en-tête à copier


        3. __init__(self: hermes_py.Header, header: int) -> None

        Crée un objet HermesHeader.

        Args:
            header: L'en-tête à copier sous forme d'entier non signé


        4. __init__(self: hermes_py.Header, remote: int, command: int, sender: int, req_res: bool) -> None

        Crée un objet HermesHeader.

        Args:
            remote: L'identifiant du destinataire
            command: L'identifiant de la commande
            sender: L'identifiant de l'envoyeur
            req_res: Drapeau Requête/Réponse

        """
    def __bytes__(self) -> bytes:
        """__bytes__(self: hermes_py.Header) -> bytes

        Convertit un HermesHeader en suite d'octets.
        """
    def __int__(self) -> int:
        """__int__(self: hermes_py.Header) -> int

        Convertit un HermesHeader en entier non signé.
        """

class Hermes:
    auto_heartbeat: bool
    default_payload_length: int
    id: int
    on_log: Callable[[Header, str, int], None] | None
    on_message: Callable[[Buffer], None] | None
    on_send: Callable[[bytes], None] | None
    on_system_message: Callable[[Buffer], None] | None
    def __init__(self, id: int) -> None:
        """__init__(self: hermes_py.Hermes, id: int) -> None

        Initialise une instance Hermès.

        Args:
            id: Identifiant Hermès de cette instance.

        """
    def handle(self) -> None:
        """handle(self: hermes_py.Hermes) -> None"""
    def init_TX(self) -> Buffer:
        """init_TX(self: hermes_py.Hermes) -> hermes_py.Buffer"""
    def input(self, payload: bytes) -> bool:
        '''input(self: hermes_py.Hermes, payload: bytes) -> bool

        Récupère et traite le fragment. 
        N\'effectue pas de vérification sur la validité du fragment et ne filtre pas sur l\'identifiant de récepteur. Ces vérifications doivent être effectués en amont.
        Cette fonction retourne faux si il n\'y a pas de buffer disponible.

        Args:
            payload: Les données lues
        Ne peut pas dépasser `constants.MAX_PAYLOAD`.

        Returns:
           True si l\'entrée est traitée, False il n\'y a plus de place disponible.

        Raises:
            RuntimeError("Too much bytes for payload"): Erreur à la conversion bytes -> payload, trop d\'octets.
            RuntimeError("Invalid payload from bytes"): Erreur à la conversion bytes -> payload, fromat invalide.

        '''
    def output(self, max_len: int) -> bytes | None:
        """output(self: hermes_py.Hermes, max_len: int) -> Optional[bytes]

        Donne un fragment à envoyer sur le support. Si il n'y a rien à envoyer, le fragment sera None.

        Args:
            max_len: Longueur maximale autorisée sur le support. Ne peut pas dépasser `constants.MAX_PAYLOAD`.

        Returns:
            Les octets à envoyer ou None
        """
    def request(self, remote: int, cmd: int, signature: str, *args) -> bool:
        """request(self: hermes_py.Hermes, remote: int, cmd: int, signature: str, *args) -> bool

        Envoie une requête à un périphérique. NON BLOQUANT.

        Args:
            remote: Identifiant du récepteur
            cmd: Identifiant de commande
            signature: Signature des arguments
            *args: Valeurs des arguments

        Returns:
            True si l'envoi est fait, False si les buffer de sortie sont pleins.
        """
    def respond(self, remote: int, cmd: int, signature: str, *args) -> bool:
        """respond(self: hermes_py.Hermes, remote: int, cmd: int, signature: str, *args) -> bool

        Envoie une réponse à un périphérique. NON BLOQUANT.

        Args:
            remote: Identifiant du récepteur
            cmd: Identifiant de commande
            signature: Signature des arguments
            *args: Valeurs des arguments

        Returns:
            True si l'envoi est fait, False si les buffer de sortie sont pleins.
        """
    def send(self, buffer: Buffer) -> None:
        """send(self: hermes_py.Hermes, buffer: hermes_py.Buffer) -> None"""
    def send_log(self, id: int, message: str, error: int = ...) -> None:
        """send_log(self: hermes_py.Hermes, id: int, message: str, error: int = 0) -> None"""
    @property
    def inputs(self) -> Manager: ...
    @property
    def outputs(self) -> Manager: ...

class Manager:
    def __init__(self) -> None:
        """__init__(self: hermes_py.Manager) -> None"""
    def alloc_buffer(self) -> Buffer | None:
        """alloc_buffer(self: hermes_py.Manager) -> Optional[hermes_py.Buffer]

        Alloue un buffer et retourne son pointeur. Si aucun buffer n'est disponible, la fonction retourne None.

        Returns:
            Un buffer, None si aucun disponible.

        """
    def alloc_prioritized_buffer(self) -> Buffer | None:
        """alloc_prioritized_buffer(self: hermes_py.Manager) -> Optional[hermes_py.Buffer]

        Alloue un buffer prioritaire et retourne son pointeur. Si aucun buffer n'est disponible, la fonction retourne None.

        Returns:
            Un buffer, None si aucun disponible.

        """
    def find_RX_buffer(self, sender: int, command: int) -> int:
        """find_RX_buffer(self: hermes_py.Manager, sender: int, command: int) -> int

        Donne l'indice du buffer correspondant aux filtres. Si aucun buffer ne correspond, la fonction renvoie `constants.HERMES_BUFFER_COUNT`.

        Args:
            sender: ID de l'envoyeur
            command: ID de la commande

        Returns:
            L'indice du buffer correspondant, `constants.HERMES_BUFFER_COUNT` sinon.
        """
    def find_RX_partial(self, remote: int, command: int, key: int) -> int:
        """find_RX_partial(self: hermes_py.Manager, remote: int, command: int, key: int) -> int

        Donne l'indice du buffer correspondant aux filtres. Si aucun buffer ne correspond, la fonction renvoie `constants.HERMES_BUFFER_COUNT`.

        Args:
            sender: ID de l'envoyeur
            command: ID de la commande
            key: Clé de transmission partielle

        Returns:
            L'indice du buffer correspondant, `constants.HERMES_BUFFER_COUNT` sinon.
        """
    def find_TX_buffer(self, remote: int, command: int) -> int:
        """find_TX_buffer(self: hermes_py.Manager, remote: int, command: int) -> int

        Donne l'indice du buffer correspondant aux filtres. Si aucun buffer ne correspond, la fonction renvoie `constants.HERMES_BUFFER_COUNT`.

        Args:
            sender: ID du destinataire
            command: ID de la commande

        Returns:
            L'indice du buffer correspondant, `constants.HERMES_BUFFER_COUNT` sinon.
        """
    def free_buffer(self, buffer: Buffer) -> None:
        """free_buffer(self: hermes_py.Manager, buffer: hermes_py.Buffer) -> None

        Libère un buffer associé à un gestionnaire.
        """
    def free_first_buffer(self) -> None:
        """free_first_buffer(self: hermes_py.Manager) -> None

        Libère le premier buffer de la file d'attente.
        """
    def free_last_buffer(self) -> None:
        """free_last_buffer(self: hermes_py.Manager) -> None

        Libère le dernier buffer de la file d'attente.
        """
    def index_of(self, buffer: Buffer) -> int:
        """index_of(self: hermes_py.Manager, buffer: hermes_py.Buffer) -> int

        Donne l'indice correspondant à un pointeur d'un buffer. Si le buffer n'existe pas dans le gestionnaire, la fonction retourne `constants.BUFFER_COUNT`.

        Args:
            buffer: le buffer dont on veut l'indice
        Returns:
            L'indice du buffer, `constants.BUFFER_COUNT` s'il n'existe pas

        """
    def mark_as_ready(self, buffer: Buffer) -> None:
        """mark_as_ready(self: hermes_py.Manager, buffer: hermes_py.Buffer) -> None

        Marque un buffer géré comme étant prêt

        Args:
            buffer: le buffer
        """
    def next(self) -> int:
        """next(self: hermes_py.Manager) -> int

        Donne l'indice du premier buffer prêt. Si aucun buffer n'est prêt, la fonction renvoie `constants.HERMES_BUFFER_COUNT`.

        Returns:
            L'indice du premier buffer prêt, `constants.HERMES_BUFFER_COUNT` si aucun buffer ne l'est.

        """
    @property
    def allocated(self) -> list[int]: ...
    @property
    def allocated_buffer_count(self) -> int: ...
    @property
    def allocated_head(self) -> int: ...
    @property
    def allocated_tail(self) -> int: ...
    @property
    def buffers(self) -> list[Buffer]: ...
    @property
    def free_buffer_count(self) -> int: ...
    @property
    def ready_buffer_count(self) -> int: ...
    @property
    def states(self) -> list[buffer_state]: ...

class buffer_state:
    key: int
    ready: bool
    rw_pos: int
    @overload
    def __init__(self) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.buffer_state) -> None

        2. __init__(self: hermes_py.buffer_state, arg0: hermes_py.buffer_state) -> None
        """
    @overload
    def __init__(self, arg0: buffer_state) -> None:
        """__init__(*args, **kwargs)
        Overloaded function.

        1. __init__(self: hermes_py.buffer_state) -> None

        2. __init__(self: hermes_py.buffer_state, arg0: hermes_py.buffer_state) -> None
        """

class constants:
    class system_cmd:
        __members__: ClassVar[dict] = ...  # read-only
        __entries: ClassVar[dict] = ...
        syscmd_error: ClassVar[constants.system_cmd] = ...
        syscmd_heartbeat: ClassVar[constants.system_cmd] = ...
        syscmd_log: ClassVar[constants.system_cmd] = ...
        syscmd_manifest: ClassVar[constants.system_cmd] = ...
        syscmd_manifest_chunk: ClassVar[constants.system_cmd] = ...
        syscmd_partial_begin: ClassVar[constants.system_cmd] = ...
        syscmd_partial_content: ClassVar[constants.system_cmd] = ...
        syscmd_partial_content_0: ClassVar[constants.system_cmd] = ...
        syscmd_partial_content_1: ClassVar[constants.system_cmd] = ...
        syscmd_partial_content_2: ClassVar[constants.system_cmd] = ...
        syscmd_partial_content_3: ClassVar[constants.system_cmd] = ...
        syscmd_system_cmd: ClassVar[constants.system_cmd] = ...
        def __init__(self, value: int) -> None:
            """__init__(self: hermes_py.constants.system_cmd, value: int) -> None"""
        def __eq__(self, other: object) -> bool:
            """__eq__(self: object, other: object) -> bool"""
        def __hash__(self) -> int:
            """__hash__(self: object) -> int"""
        def __index__(self) -> int:
            """__index__(self: hermes_py.constants.system_cmd) -> int"""
        def __int__(self) -> int:
            """__int__(self: hermes_py.constants.system_cmd) -> int"""
        def __ne__(self, other: object) -> bool:
            """__ne__(self: object, other: object) -> bool"""
        @property
        def name(self) -> str: ...
        @property
        def value(self) -> int: ...
    BUFFER_COUNT: ClassVar[int] = ...
    MAX_BUFFER_LEN: ClassVar[int] = ...
    MAX_PAYLOAD: ClassVar[int] = ...
    syscmd_error: ClassVar[constants.system_cmd] = ...
    syscmd_heartbeat: ClassVar[constants.system_cmd] = ...
    syscmd_log: ClassVar[constants.system_cmd] = ...
    syscmd_manifest: ClassVar[constants.system_cmd] = ...
    syscmd_manifest_chunk: ClassVar[constants.system_cmd] = ...
    syscmd_partial_begin: ClassVar[constants.system_cmd] = ...
    syscmd_partial_content: ClassVar[constants.system_cmd] = ...
    syscmd_partial_content_0: ClassVar[constants.system_cmd] = ...
    syscmd_partial_content_1: ClassVar[constants.system_cmd] = ...
    syscmd_partial_content_2: ClassVar[constants.system_cmd] = ...
    syscmd_partial_content_3: ClassVar[constants.system_cmd] = ...
    syscmd_system_cmd: ClassVar[constants.system_cmd] = ...
    def __init__(self, *args, **kwargs) -> None:
        """Initialize self.  See help(type(self)) for accurate signature."""

class payload:
    def __init__(self, *args, **kwargs) -> None:
        """Initialize self.  See help(type(self)) for accurate signature."""
    @staticmethod
    def valid(bytes: bytes) -> bool:
        """valid(bytes: bytes) -> bool

        Vérifie qu'un tronçon de données est valide.

        Un tronçon respecte le format :
         - Longueur >= 4
         - Bits 29 à 31 = 0
         - ID != 0

        Args:
            bytes: Le tronçon sous forme d'octets

        Returns:
            Vrai si le tronçon vérifie les conditions, faux sinon.

        Raises:
            Runtime Error: Le tronçon est trop long pour l'espace
            mémoire qui lui est réservé (flag de compilation Hermès).
        """
    @staticmethod
    def valid_filtered(bytes: bytes, self: int) -> bool:
        """valid_filtered(bytes: bytes, self: int) -> bool

        Vérifie qu'un tronçon de données est valide.

        Un tronçon respecte le format :
         - Longueur >= 4
         - Bits 29 à 31 = 0
         - ID != 0
         - remote == self

        Args:
            bytes: Le tronçon sous forme d'octets
            self: L'identifiant de récepteur

        Returns:
            Vrai si le tronçon vérifie les conditions, faux sinon.

        Raises:
            Runtime Error: Le tronçon est trop long pour l'espace
            mémoire qui lui est réservé (flag de compilation Hermès).
        """

class type:
    class types:
        __members__: ClassVar[dict] = ...  # read-only
        __entries: ClassVar[dict] = ...
        blob: ClassVar[type.types] = ...
        char: ClassVar[type.types] = ...
        double: ClassVar[type.types] = ...
        float: ClassVar[type.types] = ...
        int16: ClassVar[type.types] = ...
        int32: ClassVar[type.types] = ...
        int64: ClassVar[type.types] = ...
        int8: ClassVar[type.types] = ...
        none: ClassVar[type.types] = ...
        string: ClassVar[type.types] = ...
        def __init__(self, value: int) -> None:
            """__init__(self: hermes_py.type.types, value: int) -> None"""
        def __eq__(self, other: object) -> bool:
            """__eq__(self: object, other: object) -> bool"""
        def __hash__(self) -> int:
            """__hash__(self: object) -> int"""
        def __index__(self) -> int:
            """__index__(self: hermes_py.type.types) -> int"""
        def __int__(self) -> int:
            """__int__(self: hermes_py.type.types) -> int"""
        def __ne__(self, other: object) -> bool:
            """__ne__(self: object, other: object) -> bool"""
        @property
        def name(self) -> str: ...
        @property
        def value(self) -> int: ...
    blob: ClassVar[type.types] = ...
    char: ClassVar[type.types] = ...
    double: ClassVar[type.types] = ...
    float: ClassVar[type.types] = ...
    int16: ClassVar[type.types] = ...
    int32: ClassVar[type.types] = ...
    int64: ClassVar[type.types] = ...
    int8: ClassVar[type.types] = ...
    none: ClassVar[type.types] = ...
    string: ClassVar[type.types] = ...
    def __init__(self, *args, **kwargs) -> None:
        """Initialize self.  See help(type(self)) for accurate signature."""
